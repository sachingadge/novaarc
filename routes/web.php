<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
 */

//Route::get('/', function () {
//	return view('welcome');
//});

Auth::routes();

Route::get('/', 'HomeController@index')->name('home');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/products', 'ProductsController@index')->name('products');
Route::get('/products/add', 'ProductsController@add')->name('products/add');
Route::get('/products/edit/{id}', 'ProductsController@edit');
Route::any('/products/delete/{id}', 'ProductsController@delete');
Route::post('/products/save', 'ProductsController@save');

Route::get('/client', 'ClientController@index')->name('client');
Route::get('/client/add', 'ClientController@add')->name('client/add');
Route::get('/client/edit/{id}', 'ClientController@edit');
Route::any('/client/delete/{id}', 'ClientController@delete');
Route::post('/client/save', 'ClientController@save');

Route::get('/users', 'UsersController@index')->name('users');
Route::get('/users/add', 'UsersController@add')->name('users/add');
Route::get('/users/edit/{id}', 'UsersController@edit');
Route::any('/users/delete/{id}', 'UsersController@delete');
Route::post('/users/save', 'UsersController@save');
Route::post('/users/update', 'UsersController@update');