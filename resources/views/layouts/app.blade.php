@include('layouts.header')
@include('layouts.sidebar')
<div class="page-wrapper">
    <div class="page-breadcrumb">
        <div class="row">
            <div class="col-5 align-self-center">
                <h4 class="page-title">
                    @if(Request::segment(1))
                        {{ ucfirst(Request::segment(1)) }}
                    @endif
                </h4>
            </div>
            @if(Request::segment(1)!= 'home' )
            <div class="col-7 align-self-center">
                <div class="d-flex align-items-center justify-content-end">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item">
                                <a href="{{ route('home') }}">Home</a>
                            </li>
                            <li class="breadcrumb-item active" aria-current="page">
                            @if(Request::segment(1))
                        {{ ucfirst(Request::segment(1)) }}
                    @endif
                    </li>
                        </ol>
                    </nav>
                </div>
            </div>
            @endif
        </div>
    </div>
    <div class="container-fluid">
    
    @yield('content')

    </div>
    <footer class="footer text-center">
        All Rights Reserved by Nice admin. Designed and Developed by
        <a href="#/">NOVAARC</a>.
    </footer>
</div>
@include('layouts.footer')
