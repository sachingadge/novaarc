@extends('layouts.app')
@section('page_css')
    <!-- This page plugin CSS -->
    <link href="{{ asset('assets/extra-libs/datatables.net-bs4/css/dataTables.bootstrap4.css') }}" rel="stylesheet">
@endsection

@section('content')
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">                            
                            @if ($message = Session::get('success'))
                            <div class="alert alert-success alert-dismissible">
                                <button type="button" class="close" data-dismiss="alert">x</button>
                                    {{ $message }}
                            </div>
                            @endif   
                                <div class="table-responsive">
                                    <table id="file_export" class="table table-striped table-bordered display">
                                        <thead>
                                            <tr>
                                                <th>Id</th>
                                                <th>Name</th>
                                                <th>Email</th>
                                                <th>Role</th>                                                
                                                <th>Status</th>                                                
                                                <th>Photo</th>                                                
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($users as $val)
                                            <tr>
                                                <td>{{ $val->id }}</td>
                                                <td>{{ $val->name }}</td>                                        
                                                <td>{{ $val->email }}</td>                                        
                                                <td>{{ $val->role }}</td>                                        
                                                <td>
                                                    @if($val->status==1)
                                                    <span class="label label-success label-rounded">Active</span>
                                                    @else
                                                    <span class="label label-danger  label-rounded">Inactive</span>
                                                    @endif
                                                </td>
                                                <td>
                                                    @if($val->photo)
                                                    <img src="{{ asset('uploads/users/'.$val->photo) }}" alt="user" class="rounded-circle" width="40" height="40">
                                                    @endif
                                                </td>
                                                <td>
                                                    <a href="{{ url('/users/edit/'.$val->id) }}" class="btn waves-effect waves-light btn-sm btn-success">Edit</a>
                                                    <a href="{{ url('/users/delete/'.$val->id) }}" class="btn waves-effect waves-light btn-sm btn-danger">Delete</a>
                                                </td>
                                            </tr>  
                                        @endforeach                                          
                                        </tbody>                                       
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
@endsection

@section('page_js')
    <!--This page plugins -->
    <script src="{{ asset('assets/extra-libs/datatables.net/js/jquery.dataTables.min.js') }}"></script>
    <!-- start - This is for export functionality only -->
    <script src="{{ asset('assets/datatables/buttons/1.5.1/js/dataTables.buttons.min.js') }}"></script>
    <script src="{{ asset('assets/datatables/buttons/1.5.1/js/buttons.flash.min.js') }}"></script>
    <script src="{{ asset('ajax/libs/jszip/3.1.3/jszip.min.js') }}"></script>
    <script src="{{ asset('ajax/libs/pdfmake/0.1.32/pdfmake.min.js') }}"></script>
    <script src="{{ asset('ajax/libs/pdfmake/0.1.32/vfs_fonts.js') }}"></script>
    <script src="{{ asset('assets/datatables/buttons/1.5.1/js/buttons.html5.min.js') }}"></script>
    <script src="{{ asset('assets/datatables/buttons/1.5.1/js/buttons.print.min.js') }}"></script>
    <script src="{{ asset('dist/js/pages/datatable/datatable-advanced.init.js') }}"></script>

    <script > 
    $('.dt-buttons').prepend("<a href='{{ route('users/add') }}' class='dt-button buttons-html5 btn btn-success mr-1'><i class='fas fa-plus'></i>&nbsp; Add new</a>");
    </script>
@endsection
            