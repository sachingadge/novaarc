@extends('layouts.app')
@section('content')

<div class="row">
                    <div class="col-12">
                        <div class="card">

                            <form enctype="multipart/form-data"  class="form-horizontal needs-validation " method="post" id="product_add" action="{{ url('/users/update')}}">
                            @csrf
                            <input type="hidden" name="id" value="@if(!empty($user)){{ $user->id}}@endif">
                                <div class="card-body">
                                    <h4 class="card-title">Edit user</h4>
                                    
                                    <div class="form-group row ">
                                        <label for="status" class="col-sm-3 text-right control-label col-form-label">Status  <span class="text-danger">*</span></label>
                                        <div class="col-sm-9">
                                            
                                            <select name="status" id="status" class="form-control" required>
                                                <option value="">Select</option>                                              
                                                <option @if($user->status == 1) selected @endif value="1">Active</option>                                              
                                                <option @if($user->status == 0) selected @endif value="0">Inactive</option>                                              
                                            </select>
                                          
                                            @if($errors->has('status'))
                                            <div class="invalid-feedback">
                                                Please provide a Role of user.
                                            </div>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="form-group row ">
                                        <label for="name" class="col-sm-3 text-right control-label col-form-label">Role <span class="text-danger">*</span></label>
                                        <div class="col-sm-9">
                                            <?php

                                            $role = '';
                                            if(!empty($user)){                                                
                                                $role =  $user->role;
                                            }
                                            ?>
                                            <select name="role" id="role" class="form-control" required>
                                                <option value="">Select</option>
                                                @foreach($roles as $val)
                                                    <option @if($user->role == $val->id) selected @endif value="{{ $val->id }}">{{ $val->name }}</option>
                                                @endforeach
                                            </select>
                                          
                                            @if($errors->has('role'))
                                            <div class="invalid-feedback">
                                                Please provide a Role of user.
                                            </div>
                                            @endif
                                        </div>
                                    </div> 
                                    <div class="form-group row ">
                                        <label for="name" class="col-sm-3 text-right control-label col-form-label">Name <span class="text-danger">*</span></label>
                                        <div class="col-sm-9">
                                            <?php

                                            $name = '';
                                            if(!empty($user)){                                                
                                                $name =  $user->name;
                                            }
                                            ?>
                                            <input type="text" class="form-control @if($errors->has('name')) is-invalid @endif" id="name" name="name" value="{{ old('name', $name) }}" required>
                                            
                                            @if($errors->has('name'))
                                            <div class="invalid-feedback">
                                                Please provide a Product name.
                                            </div>
                                            @endif
                                        </div>
                                    </div> 
                                    
                                    <div class="form-group row ">
                                        <label for="name" class="col-sm-3 text-right control-label col-form-label">Email (Username) <span class="text-danger">*</span></label>
                                        <div class="col-sm-9">
                                            <?php

                                            $email = '';
                                            if(!empty($user)){                                                
                                                $email =  $user->email;
                                            }
                                            ?>
                                           <input readonly id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email', $email) }}" required autocomplete="email">

                                            @error('email')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                    </div>  
                                    <div class="form-group row ">
                                    <?php
                                         $short_description = '';
                                            if(!empty($user)){                                                
                                                $short_description =  $user->short_description;
                                            }
                                    ?>
                                        <label for="short_description" class="col-sm-3 text-right control-label col-form-label">Change Password</label>
                                        <div class="col-sm-9">
                                        <input id="password" type="text" class="form-control @error('password') is-invalid @enderror" name="password"  autocomplete="new-password">

                                            @error('password')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                    </div> 
                                                                                                           
                                    <div class="form-group row @if($errors->has('photo')) needs-validation @endif">
                                        <label class="col-sm-3 text-right control-label col-form-label">Change photo </label>
                                        <div class="col-sm-9">
                                                <img src="{{ asset('uploads/users/'.$user->photo) }}" alt="user" class="rounded-circle" width="100" height="100">

                                            <div class="input-group mb-3">
                                                <div class="input-group-prepend">
                                                    <span class="input-group-text">Upload</span>
                                                </div>
                                                <div class="custom-file">
                                                    <input type="file" class="custom-file-input @if($errors->has('photo')) is-invalid @endif" name="photo" id="photo">
                                                    
                                                    <label class="custom-file-label" for="photo">Choose file</label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>                                   
                                    
                                </div>
                                <hr>
                                <div class="card-body">
                                    <div class="form-group m-b-0 text-right">
                                        <button type="submit" class="btn btn-info waves-effect waves-light">Save</button>
                                        <button type="submit" class="btn btn-dark waves-effect waves-light">Cancel</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
@endsection
@section('page_js')
<script type="text/javascript">
    $("input").change(function () {
        $(this).removeClass("is-invalid");
        $(this).next().empty();
    });
</script>
@endsection
