@extends('layouts.app')
@section('content')

<div class="row">
                    <div class="col-12">
                        <div class="card">

                            <form enctype="multipart/form-data"  class="form-horizontal needs-validation " method="post" id="product_add" action="{{ url('/products/save')}}">
                            @csrf
                            <input type="hidden" name="id" value="@if(!empty($product)){{ $product->id}}@endif">
                                <div class="card-body">
                                    <h4 class="card-title">Add New</h4>
                                    <div class="form-group row ">
                                        <label for="name" class="col-sm-3 text-right control-label col-form-label">Name <span class="text-danger">*</span></label>
                                        <div class="col-sm-9">
                                            <?php

                                            $name = '';
                                            if(!empty($product)){                                                
                                                $name =  $product->name;
                                            }
                                            ?>
                                            <input type="text" class="form-control @if($errors->has('name')) is-invalid @endif" id="name" name="name" value="{{ old('name', $name) }}">
                                            
                                            @if($errors->has('name'))
                                            <div class="invalid-feedback">
                                                Please provide a Product name.
                                            </div>
                                            @endif
                                        </div>
                                    </div>                                    
                                    <!-- <div class="form-group row">
                                        <label class="col-sm-3 text-right control-label col-form-label">Budget</label>
                                        <div class="col-sm-9">
                                            <select class="form-control">
                                                <option>Choose Your Option</option>
                                                <option>Less then $5000</option>
                                                <option>$5000 - $10000</option>
                                                <option>$10000 - $20000</option>
                                            </select>
                                        </div>
                                    </div> -->
                                    <div class="form-group row @if($errors->has('photo')) needs-validation @endif">
                                        <label class="col-sm-3 text-right control-label col-form-label">Photo <span class="text-danger">*</span></label>
                                        <div class="col-sm-9">
                                            <div class="input-group mb-3">
                                                <div class="input-group-prepend">
                                                    <span class="input-group-text">Upload</span>
                                                </div>
                                                <div class="custom-file">
                                                    <input type="file" class="custom-file-input @if($errors->has('photo')) is-invalid @endif" name="photo" id="photo">
                                                    
                                                    <label class="custom-file-label" for="photo">Choose file</label>
                                                </div>
                                             
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group row ">
                                    <?php
                                         $short_description = '';
                                            if(!empty($product)){                                                
                                                $short_description =  $product->short_description;
                                            }
                                    ?>
                                        <label for="short_description" class="col-sm-3 text-right control-label col-form-label">Short Description <span class="text-danger">*</span></label>
                                        <div class="col-sm-9">
                                            <input type="text" class="form-control @if($errors->has('short_description')) is-invalid @endif" name="short_description" id="short_description" value="{{ old('short_description', $short_description) }}">
                                            @if($errors->has('short_description'))
                                            <div class="invalid-feedback">
                                                Please provide a Product Short Description.
                                            </div>
                                            @endif
                                        </div>
                                    </div>                                    
                                    <div class="form-group row">
                                    <?php
                                         $description = '';
                                            if(!empty($product)){                                                
                                                $description =  $product->description;
                                            }
                                    ?>
                                        <label for="description" class="col-sm-3 text-right control-label col-form-label">Description</label>
                                        <div class="col-sm-9">
                                            <textarea name="description" id="description" rows="5" class="form-control ">{{ old('description', $description) }}</textarea>
                                        </div>
                                    </div>
                                </div>
                                <hr>
                                <div class="card-body">
                                    <div class="form-group m-b-0 text-right">
                                        <button type="submit" class="btn btn-info waves-effect waves-light">Save</button>
                                        <button type="submit" class="btn btn-dark waves-effect waves-light">Cancel</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
@endsection
@section('page_js')
<script type="text/javascript">
    $("input").change(function () {
        $(this).removeClass("is-invalid");
        $(this).next().empty();
    });
</script>
@endsection
