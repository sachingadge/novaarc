@extends('layouts.app')
@section('content')

<div class="row">
                    <div class="col-12">
                        <div class="card">
                            
                            <form class="form-horizontal" method="post" action="{{ route('products/save') }}">
                            @csrf
                                <div class="card-body">
                                    <h4 class="card-title">Add New</h4>
                                    <div class="form-group row">
                                        <label for="name" class="col-sm-3 text-right control-label col-form-label">Name</label>
                                        <div class="col-sm-9">
                                            <input type="text" class="form-control" id="name" name="name" value="{{ old('name') }}">
                                        </div>
                                    </div>                                    
                                    <!-- <div class="form-group row">
                                        <label class="col-sm-3 text-right control-label col-form-label">Budget</label>
                                        <div class="col-sm-9">
                                            <select class="form-control">
                                                <option>Choose Your Option</option>
                                                <option>Less then $5000</option>
                                                <option>$5000 - $10000</option>
                                                <option>$10000 - $20000</option>
                                            </select>
                                        </div>
                                    </div> -->
                                    <div class="form-group row">
                                        <label class="col-sm-3 text-right control-label col-form-label">Select File</label>
                                        <div class="col-sm-9">
                                            <div class="input-group mb-3">
                                                <div class="input-group-prepend">
                                                    <span class="input-group-text">Upload</span>
                                                </div>
                                                <div class="custom-file">
                                                    <input type="file" class="custom-file-input" id="photo">
                                                    <label class="custom-file-label" for="photo">Choose file</label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="abpro" class="col-sm-3 text-right control-label col-form-label">Short Description</label>
                                        <div class="col-sm-9">
                                            <input type="text" class="form-control" id="abpro" >
                                        </div>
                                    </div>                                    
                                    <div class="form-group row">
                                        <label for="abpro" class="col-sm-3 text-right control-label col-form-label">Description</label>
                                        <div class="col-sm-9">
                                            <textarea name="description" id="description" rows="5" class="form-control"></textarea>
                                        </div>
                                    </div>
                                </div>
                                <hr>
                                <div class="card-body">
                                    <div class="form-group m-b-0 text-right">
                                        <button type="submit" class="btn btn-info waves-effect waves-light">Save</button>
                                        <button type="submit" class="btn btn-dark waves-effect waves-light">Cancel</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
@endsection
