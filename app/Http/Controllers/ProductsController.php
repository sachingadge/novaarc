<?php
use Illuminate\Http\Request;
namespace App\Http\Controllers;
use App\Products; 

use Illuminate\Http\Request;

class ProductsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $data['products'] =Products::all();
        return view('products.index', $data);
    }
    public function add()
    {
        return view('products.add');        
    }
    public function edit($id)
    {
        $data['product'] =Products::find($id);
        return view('products.add', $data);
    }
    public function save(Request $request)
    {
       // dd( $request->post());
       $validatedData = $request->validate([
            'name' => 'required',
            'short_description' => 'required',
            'photo' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',        

        ]);
        $save =  array(
            'name'=> $request->post('name'),
            'short_description'=> $request->post('short_description'),
            'description'=> $request->post('description'),
        );

        
        if($request->hasfile('photo')) 
        {
            $file = $request->file('photo');
            $extension = $file->getClientOriginalExtension(); // getting image extension
            $filename =time().'.'.$extension;
            $file->move('uploads/products/', $filename);
            $save['photo'] = $filename;
        }
       if(!empty( $request->post('id') ) ){
            Products::where('id',  $request->post('id') )
            ->update($save);            
            $msg = 'Product update successfully.';
       }else{
           
		Products::create($save);
        $msg = 'Product created successfully.';
       }
		return redirect('products')->with('success', $msg);
    }
    public function delete($id)
    {
        
        $delete = Products::destroy($id);
        if (!is_null($delete)) {
            $msg = 'Record deleted successfully!';
        } else {
            $msg = 'Something went wrong!';            
        }
        return redirect('products')->with('success', $msg);
    }
}
