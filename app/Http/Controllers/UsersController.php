<?php
use Illuminate\Http\Request;
namespace App\Http\Controllers;
use App\User; 
use App\Role; 
use Illuminate\Support\Facades\Hash;

use Illuminate\Http\Request;

class UsersController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $data['users'] =User::select('users.id','users.name','users.email','users.status','roles.name as role' )->leftjoin('roles', 'users.role', '=', 'roles.id')->orderBy('id', 'desc')->get();
        $data['roles'] = Role::all();
       // print_r($data['users']);exit;
        //dd($data);
        return view('users.index', $data);
    }
    public function add()
    {
        $data['roles'] = Role::all();
        
        return view('users.add', $data);        
    }
    public function edit($id)
    {
        $data['user'] =User::find($id);
        if(empty($data['user'])){
            $msg = 'Something went wrong!';            
            return redirect('users')->with('error', $msg);    
        }
        $data['roles'] = Role::all();
        //dd($data);
        return view('users.edit', $data);
    }
    public function save(Request $request)
    {
       // dd( $request->post());
        $validatedData = $request->validate([
            'name' => 'required',		
            'photo' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'role' => 'required',
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:6'],        
            	
        ]);
        $save =  array(
            'name'=> $request->post('name'),           
            'role'=> $request->post('role'),           
            'email'=> $request->post('email'),           
            'password'=> Hash::make($request->post('password')),           
        );

        
        if($request->hasfile('photo')) 
        {
            $file = $request->file('photo');
            $extension = $file->getClientOriginalExtension(); // getting image extension
            $filename =time().'.'.$extension;
            $file->move('uploads/users/', $filename);
            $save['photo'] = $filename;
        }
       if(!empty( $request->post('id') ) ){
        User::where('id',  $request->post('id') )
        ->update($save);
        $msg = 'User update successfully.';

       }else{
       // dd($save);
		User::create($save);
        $msg = 'User created successfully.';
       }
		return redirect('users')->with('success', $msg);
    }
    
    public function update(Request $request)
    {
       // dd( $request->post());
        $validatedData = $request->validate([
            'name' => 'required',		
            'photo' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'role' => 'required',            
            'status' => 'required',            
        
            	
        ]);
        $save =  array(
            'name'=> $request->post('name'),           
            'role'=> $request->post('role'),            
            'status'=> $request->post('status'),            
        );       
        if(!empty($request->post('password'))) 
        {
            $save['password'] = Hash::make($request->post('password'));
        }
        
        if($request->hasfile('photo')) 
        {
            $file = $request->file('photo');
            $extension = $file->getClientOriginalExtension(); // getting image extension
            $filename =time().'.'.$extension;
            $file->move('uploads/users/', $filename);
            $save['photo'] = $filename;
        }
       if(!empty( $request->post('id') ) ){
        User::where('id',  $request->post('id') )
        ->update($save);
        $msg = 'User update successfully.';

       }else{
       // dd($save);
		User::create($save);
        $msg = 'User created successfully.';
       }
		return redirect('users')->with('success', $msg);
    }
    public function delete($id)
    {
        
        $delete = User::destroy($id);
        if (!is_null($delete)) {
            $msg = 'Record deleted successfully!';
        } else {
            $msg = 'Something went wrong!';            
        }
        return redirect('users')->with('success', $msg);
    }
}
