<?php
use Illuminate\Http\Request;
namespace App\Http\Controllers;
use App\Client; 

use Illuminate\Http\Request;

class ClientController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $data['client'] =Client::all();
        return view('client.index', $data);
    }
    public function add()
    {
        return view('client.add');        
    }
    public function edit($id)
    {
        $data['client'] =Client::find($id);
        return view('client.add', $data);
    }
    public function save(Request $request)
    {
       // dd( $request->post());
        $validatedData = $request->validate([
            'name' => 'required',		
			'photo' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            	
        ]);
        $save =  array(
            'name'=> $request->post('name'),           
        );

        
        if($request->hasfile('photo')) 
        {
            $file = $request->file('photo');
            $extension = $file->getClientOriginalExtension(); // getting image extension
            $filename =time().'.'.$extension;
            $file->move('uploads/clients/', $filename);
            $save['photo'] = $filename;
        }
       if(!empty( $request->post('id') ) ){
        Client::where('id',  $request->post('id') )
        ->update($save);
        $msg = 'Client update successfully.';

       }else{
    
		Client::create($save);
        $msg = 'Client created successfully.';
       }
		return redirect('client')->with('success', $msg);
    }
    public function delete($id)
    {
        
        $delete = Client::destroy($id);
        if (!is_null($delete)) {
            $msg = 'Record deleted successfully!';
        } else {
            $msg = 'Something went wrong!';            
        }
        return redirect('client')->with('success', $msg);
    }
}
